﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, zMin, zMax;
}

public class PlayerCoontroller : MonoBehaviour {

	public float speedY;
	public float speedX;
	public Boundary boundary;
	public float tilt;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;

	private float nextFire;
	Rigidbody rb;

	void Start (){
		rb = GetComponent<Rigidbody> ();
	}

	void Update (){

		if (Input.GetKeyDown (KeyCode.LeftArrow))
			rb.velocity = new Vector3 (-speedX, 0.0f, rb.velocity.y);

		if (Input.GetKeyDown (KeyCode.RightArrow))
			rb.velocity = new Vector3 (speedX, 0.0f, rb.velocity.y);
	}

	void FixedUpdate(){
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		rb.velocity= new Vector3 (rb.velocity.x, 0.0f, moveVertical * speedY);
		rb.position = new Vector3 (
			Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax), 
			0.0f, 
			Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
		);

			
		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt );
	}
}
