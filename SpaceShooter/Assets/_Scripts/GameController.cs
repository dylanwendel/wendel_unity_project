﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public GameObject[] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public GUIText scoreText;
	public GUIText highScoreText;
	private int score;
	public GUIText restartText;
	public GUIText gameOverText;
	private bool gameOver;
	private bool restart;
	private int highScore;
	private float startTime;




	void Start () {
		score = 0;
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		updateScore ();
		StartCoroutine (spawnWaves ());
		startTime = Time.time;

		highScore = PlayerPrefs.GetInt ("highScore", 0);
		highScoreText.text = "High Score: " + highScore;
	}

	void Update () {
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);


			}
		}

		AddYards ();

	}

	void AddYards() {

		if (gameOver)
			return;

		float timePassed = Time.time - startTime;
		int yards = Mathf.FloorToInt (timePassed* 50);
		yards = yards;
		score = yards;
		updateScore ();

		if (score > highScore) {
			highScore = score;
			PlayerPrefs.SetInt ("highScore", highScore);
			highScoreText.text = "High Score: " + highScore;
		}

	}

	IEnumerator spawnWaves () {
		yield return new WaitForSeconds (startWait);
		while (true){
			for (int i = 0; i < hazardCount; i++)
			{
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	/*public void addScore (int newScoreValue){
		score += newScoreValue;
		updateScore ();

		if (score > highScore) {
			highScore = score;
			PlayerPrefs.SetInt ("highScore", highScore);
			highScoreText.text = "High Score: " + highScore;
		}
	}*/

	void updateScore () {
		scoreText.text = "Score: " + score;

	}

	public void GameOver() {
		gameOverText.text = "Game Over";
		gameOver = true;



	}
}
